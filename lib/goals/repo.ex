defmodule Goals.Repo do
  use Ecto.Repo,
    otp_app: :goals,
    adapter: Ecto.Adapters.Postgres
end
