defmodule GoalsWeb.PageController do
  use GoalsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
