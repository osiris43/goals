# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :goals,
  ecto_repos: [Goals.Repo]

# Configures the endpoint
config :goals, GoalsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "T20HSfKT7X1WzBfPfvHNL88Ksi3lUNB++b36JXXJEDEl1Q/MuhcrP7aW+GeOcqew",
  render_errors: [view: GoalsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Goals.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
